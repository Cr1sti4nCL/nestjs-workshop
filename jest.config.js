const { getJestProjects } = require('@nrwl/jest');

module.exports = {
  proyects: ['<rootDir>', ...getJestProjects()],
};

import { consumeApiNameResponse } from '../model/consumeApiNameAdapter.model';

export interface ConsumeApiNamePort {
  getAnotherApiResponse(): Promise<consumeApiNameResponse>;
}

//-->tanque a pedales
//--> 3 tier layer

import { consumeApiNameResponse } from '../../domain/model/consumeApiNameAdapter.model';
import { ConsumeApiNamePort } from '../../domain/port/consumeApiName.port';
import axios from 'axios';
import { InternalServerErrorException } from '@nestjs/common';

export class consumeApiNameAdapter implements ConsumeApiNamePort {
  getAnotherApiResponse(): Promise<consumeApiNameResponse> {
    return axios
      .get('https://catfact.ninja/fact')
      .then((axiosRes) => {
        const result = new consumeApiNameResponse(
          axiosRes.data.fact,
          axiosRes.data.length
        );
        return result;
      })
      .catch(() => {
        throw new InternalServerErrorException(
          'error response on getAnotherApiResponse'
        );
      });
  }
}

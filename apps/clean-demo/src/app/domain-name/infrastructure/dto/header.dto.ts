import { ApiProperty } from '@nestjs/swagger';
import { Expose, Type } from 'class-transformer';
import { IsDefined } from 'class-validator';

//TODO: EXAMPLE TO USE WITH HEADER PARAMS
export class HeadersDto {
  @ApiProperty({ example: 'web' })
  @IsDefined()
  @Expose({ name: 'channel' })
  'channel': string;

  @ApiProperty({ example: '84266fdbd31d4c2c6d0665f7e8380fa3' })
  @IsDefined()
  @Expose({ name: 'sessionid' })
  'sessionid': string;
}

//TODO: EXAMPLE TO USE WITH BODY REQUEST PARAMS
export class BodyRequestDto {
  @ApiProperty({ example: '' })
  @Type(() => String)
  @IsDefined()
  @Expose({ name: 'param1' })
  param1: string;

  @ApiProperty({ example: 0 })
  @Type(() => Number)
  @IsDefined()
  @Expose({ name: 'param2' })
  param2: number;

  @ApiProperty({ example: '' })
  @Type(() => String)
  @IsDefined()
  @Expose({ name: 'param3' })
  param3: string;

  @ApiProperty({ example: true })
  @Type(() => Boolean)
  @IsDefined()
  @Expose({ name: 'param4' })
  param4: boolean;
}

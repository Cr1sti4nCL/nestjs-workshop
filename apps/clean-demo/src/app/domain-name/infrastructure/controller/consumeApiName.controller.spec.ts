import { Test, TestingModule } from '@nestjs/testing';
import { of, Observable, throwError } from 'rxjs';
import { InternalServerErrorException } from '@nestjs/common';

/* const MockGetAnotherApiResponse: consumeApiNameResponse = {
  fact: 'resultado exitoso',
  length: 0,
}; */

describe('consumeApiNameService', () => {
  let app: TestingModule;
  /*   let canPort: ConsumeApiNamePort;
  let canService: consumeApiNameService;
 */
  /*  beforeAll(async () => {
    app = await Test.createTestingModule({
      imports: [],
      providers: [
        consumeApiNameService,
        {
          provide: 'ConsumeApiNamePort',
          useClass: consumeApiNameAdapter,
        },
      ],
    }).compile();
    canPort = app.get<ConsumeApiNamePort>('ConsumeApiNamePort');
    canService = app.get<consumeApiNameService>(consumeApiNameService);
  });
 */
  afterEach(async () => {
    await app.close();
  });

  /*  describe('obtainAnotherApiResponse Success', () => {
    it('should return a valid response when the call is successfull', async () => {
      canPort.getAnotherApiResponse = jest.fn(() =>
        Promise.resolve(MockGetAnotherApiResponse)
      );

      const result = await canService.obtainAnotherApiResponse();

      expect(result.fact).toBe('resultado exitoso');
      expect(result.length).toEqual(0);
    });

    it('should return a fail response when the call throws error', async () => {
      canPort.getAnotherApiResponse = jest.fn(() =>
        Promise.reject(
          new InternalServerErrorException(
            'error response on getAnotherApiResponse'
          )
        )
      );
      const result = await canService
        .obtainAnotherApiResponse()
        .catch((error) => {
          expect(error.message).toBe('error response on getAnotherApiResponse');
          expect(error).toBeInstanceOf(InternalServerErrorException);
        });
    });
  }); */
});

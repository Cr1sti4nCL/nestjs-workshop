import { Body, Controller, Get, HttpCode, Post } from '@nestjs/common';
import { RequestHeader } from '../../../../../../clean-demo/src/helpers/header.validator.helper.decorator';
import { consumeApiNameService } from '../../application/consumeApiName.service';
import { BodyRequestDto, HeadersDto } from '../dto/header.dto';

@Controller()
export class ConsumeApiNameController {
  constructor(private service: consumeApiNameService) {}

  //#region Region CONTROLLER

  @Get('v1.0/consume-api-name')
  @HttpCode(200)
  getConsumeApiName(@RequestHeader(HeadersDto) headers: HeadersDto) {
    console.log('headers GET', headers);
    return this.service.obtainAnotherApiResponse();
  }

  @Post('v1.0/consume-api-name')
  @HttpCode(200)
  postConsumeApiName(
    @RequestHeader(HeadersDto) headers: HeadersDto,
    @Body() body: BodyRequestDto
  ) {
    console.log('headers POST', headers);
    return this.service.createAnotherApiResponse(body);
  }

  //#endregion Region CONTROLLER
}

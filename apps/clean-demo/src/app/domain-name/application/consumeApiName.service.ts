import { Inject, Injectable } from '@nestjs/common';
import { consumeApiNameResponse } from '../domain/model/consumeApiNameAdapter.model';
import { ConsumeApiNamePort } from '../domain/port/consumeApiName.port';

@Injectable()
export class consumeApiNameService {
  constructor(
    @Inject('ConsumeApiNamePort') private readonly port: ConsumeApiNamePort
  ) {}

  obtainAnotherApiResponse(): Promise<consumeApiNameResponse> {
    return this.port.getAnotherApiResponse();
  }

  createAnotherApiResponse(body) {
    console.log('body', body);
    return { status: 'success', payload: { data: 'data ok' } };
  }
}

import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { consumeApiNameService } from './domain-name/application/consumeApiName.service';
import { consumeApiNameAdapter } from './domain-name/infrastructure/adapter/consume-api-name.adapter';
import { ConsumeApiNameController } from './domain-name/infrastructure/controller/consumeApiName.controller';

@Module({
  imports: [],
  controllers: [AppController, ConsumeApiNameController],
  providers: [
    AppService,
    consumeApiNameService,
    {
      provide: 'ConsumeApiNamePort',
      useClass: consumeApiNameAdapter,
    },
  ],
})
export class AppModule {}
